# DB
import sqlite3
conn = sqlite3.connect('data.db')
c = conn.cursor()

table_users = """
    CREATE TABLE IF NOT EXISTS Users(
        user_id INTEGER PRIMARY KEY NOT NULL,
        name char(40) NOT NULL,
        username char(40) NOT NULL,
        password text NOT NULL
    )
"""

table_search = """
    CREATE TABLE IF NOT EXISTS Search(
        search_id INTEGER PRIMARY KEY NOT NULL,
        age int(32) NOT NULL,
        grlivarea int(32) NOT NULL,
        lotfrontage int(32) NOT NULL,
        lotarea int(32) NOT NULL,
        garagearea int(32) NOT NULL,
        fence int(32) NOT NULL,
        pool int(32) NOT NULL,
        estimated_price int(32) NOT NULL,
        user_id INTEGER,
        CONSTRAINT fk_users
            FOREIGN KEY (user_id)
            REFERENCES Users(user_id)
    );
"""

# Functions
def create_table_search():
	c.execute(table_search)

# Functions
def create_table_users():
	c.execute(table_users)

create_table_users()
create_table_search()

def delete_user():
    c.execute('DELETE FROM Users WHERE user_id=1')
    conn.commit()

def create_user():
    c.execute('INSERT INTO Users(user_id, name, username, password) VALUES(2,"admin", "admin", "admin");')
    conn.commit()
    print('User created')

#delete_user()
#create_user()