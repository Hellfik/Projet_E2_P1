import streamlit as st
import streamlit_authenticator as stauth
import pandas as pd
import pickle
from pathlib import Path
import bcrypt
import re
import streamlit_authenticator as stauth
from functions import user_input_features, make_prediction
# DB
import sqlite3
conn = sqlite3.connect('data.db')
c = conn.cursor()


model = "../model/finalized_model.sav"

def get_username_in_db():
    sql = "SELECT username from USERS"
    usermanes_db = c.execute(sql)

    usernames = []
    for user in usermanes_db:
        usernames.append(user[0])
    print(usernames)
    return usernames


def register_page():
    st.title('Registration Form')
    # creating a form
    my_form=st.form(key='form-1', clear_on_submit=True)
    # creating input fields
    with my_form:
        fname=my_form.text_input('Name:')
        lname=my_form.text_input('Username:')
        password=my_form.text_input('Password:', type="password")
        submitted=my_form.form_submit_button('Register')

        if submitted:
            if len(password) < 8:
                st.error("Password too short")
            elif lname in get_username_in_db():
                st.error("Username already exists")
            else:
                bytes = password.encode('utf-8')
                salt = bcrypt.gensalt()
                hash = bcrypt.hashpw(bytes, salt)
                sql = f'INSERT INTO Users(name, username, password) VALUES("{fname}", "{lname}", "{hash.decode()}");'
                c.execute(sql)
                conn.commit()
                st.success("Successfully registered!!")


def login_page():

    db_users_name = c.execute('SELECT name FROM Users')
    names = []
    for user in db_users_name:
        names.append(user[0])

    db_usernames= c.execute('SELECT username FROM Users')
    usernames = []
    for user in db_usernames:
        usernames.append(user[0])
        
    db_passwords = c.execute('SELECT password FROM Users')
    passwords = []
    for user in db_passwords:
        passwords.append(user[0])

    authenticator = stauth.Authenticate(
        names,
        usernames, 
        passwords,
        "ame_house",
        "cookie",
        cookie_expiry_days=30,
    )

    name, authentication_status, username = authenticator.login('Login', 'main')
    if 'authentication_status' not in st.session_state:
        st.session_state['authentication_status'] = authentication_status

    if st.session_state['authentication_status']:
        st.write("""
        # Prédiction du prix de vente des biens immobiliers à Ames (Iowa USA)
        """)
        st.write('---')
        # Sidebar
        # Header of Specify Input Parameters
        authenticator.logout("Logout", "sidebar")
        st.sidebar.title(f"Welcome {name}")
        st.sidebar.header('Quels sont vos critères?')
        df = user_input_features()
        st.header('Prediction du prix de vente')

        if not df.empty:
            # Print specified input parameters
            st.header('Précisez vos critères')
            st.write(df)
            st.write('---')
            # creating a form
            my_form=st.form(key='form-2', clear_on_submit=True)
            submitted=my_form.form_submit_button('Make a prediction')
            sql_user_id = f"SELECT user_id FROM Users WHERE username = '{username}';"
            user_id = c.execute(sql_user_id).fetchall()
            searches = f"SELECT * FROM Search WHERE user_id = '{list(user_id[0])[0]}';"
            query = c.execute(searches).fetchall()
            st.title('Historic of searches')
            search_df = pd.DataFrame(query, index=None, columns=['search_id','age','grlivarea','lotfrontage', 'lotarea' ,'garagearea', 'fence', 'pool', 'estimated_price', 'user_id' ])
            search_df = search_df[['age', 'grlivarea', 'lotfrontage', 'lotarea', 'garagearea', 'fence', 'pool', 'estimated_price']]
            if search_df.empty:
                st.write('You don\'t have any research yet, make a one !!')
            else:
                st.write(search_df.style.hide_index())
            if submitted:
                text_predict, prediction = make_prediction(df, model)
                st.write(text_predict)
                age = df.iloc[0]['Age']
                grlivarea = df.iloc[0]['GrLivArea']
                lotarea = df.iloc[0]['LotArea']
                lotfrontage = df.iloc[0]['LotFrontage']
                garagearea = df.iloc[0]['GarageArea']
                fence = df.iloc[0]['Fence']
                pool = df.iloc[0]['Pool']
                sql_insert_search = f"INSERT INTO Search(age, grlivarea, lotarea, lotfrontage ,garagearea, fence, pool, estimated_price, user_id) VALUES('{age}', '{grlivarea}', '{lotarea}', '{lotfrontage}', '{garagearea}', '{fence}', '{pool}', '{prediction}' ,'{list(user_id[0])[0]}');"
                c.execute(sql_insert_search)
                conn.commit()
            st.write('---')
    elif st.session_state['authentication_status'] == False:
        st.markdown("# Welcome to the house price prediction tool 🎈")
        st.sidebar.markdown("# Please login for full experience 🎈")
        st.error('Username/password is incorrect')
    elif st.session_state['authentication_status'] == None:
        st.markdown("# Welcome to the house price prediction tool 🎈")
        st.sidebar.markdown("# Please login for full experience 🎈")
        st.warning('Please enter your username and password')

#login_page()

page_names_to_funcs = {
    "Home Page": login_page,
    "Register Page": register_page,
}

selected_page = st.sidebar.selectbox("Select a page", page_names_to_funcs.keys())
page_names_to_funcs[selected_page]()