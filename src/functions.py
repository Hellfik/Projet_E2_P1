import streamlit as st
import pandas as pd
import pickle

def load_csv(file_path):
    """
    Load CSV file and print out the execution time

    Parameter : CSV file path

    Return : Returns a Dataframe
    """
    df = pd.read_csv(file_path, low_memory=False, nrows = None)

    return df

X = load_csv("../data/intermediate/clean_X.csv")
def user_input_features():
    """Function to set variables needed for the ML model

    Returns:
        [type]: [Dataframe]
    """
    Age = st.sidebar.slider('Ancienneté du bien', int(X.Age.min()), int(X.Age.max()), int(X.Age.mean()))
    LotArea = st.sidebar.slider('Surface totale', int(X.LotArea.min()), int(X.LotArea.max()), int(X.LotArea.mean()))
    GrLivArea = st.sidebar.slider('Surface au sol', int(X.GrLivArea.min()), int(X.GrLivArea.max()), int(X.GrLivArea.mean()))
    LotFrontage = st.sidebar.slider('Taille de la façade', int(X.LotFrontage.min()), int(X.LotFrontage.max()), int(X.LotFrontage.mean()))
    GarageArea = st.sidebar.slider('Taille du garage', int(X.GarageArea.min()), int(X.GarageArea.max()), int(X.GarageArea.mean()))
    Fence = st.sidebar.select_slider('Présence de barrières', options=[False, True])
    Pool = st.sidebar.select_slider('Piscine souhaitée?', options=[False, True])

    data = {'Age': Age,
            'GrLivArea': GrLivArea,
            'LotFrontage': LotFrontage,
            'LotArea': LotArea,
            'GarageArea': GarageArea,
            'Fence': Fence,
            'Pool' : Pool
            }
    features = pd.DataFrame(data, index=[0])
    return features


# Apply Model to Make Prediction
def make_prediction(df, model):
    """Function that make the prediction based on user inputs

    Args:
        df ([Dataframe]): [Dataframe with all variables]
        model ([path]): [Path to the model]

    Returns:
        [string]: [Price prediction]
    """
    loaded_model = pickle.load(open(model, 'rb'))
    prediction = loaded_model.predict(df)

    return 'With these informations, the house is estimated at ${:,}'.format(int(prediction)), int(prediction)