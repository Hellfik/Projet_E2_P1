from functions import user_input_features, make_prediction
import os
import sqlite3
import pytest
import pandas as pd
import numpy as np

myconn = sqlite3.connect('data.db')
model = "../model/finalized_model.sav"


def dataDir():
    FIXTURE_DIR = os.path.join(
        os.path.dirname(os.path.realpath(__file__)),
        'data/intermediate',
    )
    return FIXTURE_DIR

def modelDir():
    FIXTURE_DIR = os.path.join(
        os.path.dirname(os.path.realpath(__file__)),
        'model',
    )
    return FIXTURE_DIR


def data_test():
    FIXTURE = {
        'Age': 50,
        'GrLivArea': 200,
        'LotFrontage': 200,
        'LotArea': 200,
        'GarageArea': 50,
        'Fence': True,
        'Pool': True
    }
    data = pd.DataFrame(FIXTURE, index=[0])

    return data


class TestStreamlit:
    def test_user_input(self):
        """
            Test user inputs/ if dataframe not empty
        """
        features = user_input_features()
        assert features.empty == False

    def test_import_data(self):
        """
            Test the existence of clean_X.csv file
        """
        os.path.isfile(f'{dataDir()}/clean_X.csv') == True

    def test_import_model(self):
        """
            Test the existence of finalized_model.sav
        """
        os.path.isfile(f'{modelDir()}/finalized_model.sav') == True


    def test_db_conn(self):
        """
            Test the connection to the database
        """
        success_message = ""
        try:
            myconn.cursor()
            success_message = "success"
        except Exception as ex:
            return False

        assert success_message == "success"

        
    def test_prediction(self):
        """
            Test the model
        """
        data = data_test()
        pred = make_prediction(data, model)
        assert type(pred) == tuple
